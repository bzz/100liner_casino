/**
* Copyright (c) 2017 Mikhail Baynov
* Distributed under the GNU GPL v2
*/
pragma solidity ^0.4.11;
contract Casino {
    mapping (uint8 => address) public users;
    mapping (address => uint256) public balances;

    uint8[] public players;
    uint256 public jackpot;
    uint256 roll_block_number;


    function Casino() {
    }

    function() payable {
        if (msg.value > 0) {
            balances[msg.sender] += msg.value;

            uint8 i = 0;
            while (users[i] != 0 && users[i] != msg.sender) {
                if (balances[users[i]] == 0 && jackpot == 0) break;
                ++i;
            }
            users[i] = msg.sender;
        }
    }


    function roll() {
        if (jackpot > 0) {
            balances[winner()] += jackpot;
            jackpot = 0;
            players.length = 0;
        }
        roll_block_number = block.number;

        uint256 b = 0;
        uint8 players_count = 0;
        uint8 i = 0;
        while (users[i] != 0) {
            if (balances[users[i]] > 0) {
                if (b == 0 || b > balances[users[i]]) {
                    b = balances[users[i]];
                }
                players.push(i);//[players_count] = i;
                ++players_count;
            }
            ++i;
        }

        if (players_count > 1) {
        i = 0;
            while (i < players.length) {
                if (balances[users[players[i]]] > 0) {
                    balances[users[players[i]]] -= b;
                }
                ++i;
            }
            jackpot = b * players.length;
        }
    }


    function winner() constant returns (address) {
        bytes32 hash = block.blockhash(roll_block_number);
        if (players.length > 1 && hash > 0) {
            uint8 winner = players[uint256(hash) % players.length];
            return users[winner];
        } else return 0;
    }
    
    function balance() constant returns (uint256) {
        if (msg.sender == winner()) {
            return balances[msg.sender] + jackpot;
        } else return balances[msg.sender];
        return 0;
    }

    function withdraw() {
        if (msg.sender == winner()) {
            msg.sender.transfer(balances[msg.sender] + jackpot);
            jackpot = 0;
        } else msg.sender.transfer(balances[msg.sender]);
        balances[msg.sender] = 0;
    }
}
