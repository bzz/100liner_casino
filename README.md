# README #

With Ethereum smart blockchain technology, honest gambling became possible. Just buy (or mine) some Ethereum, deploy this contract and have your mates playing with you, all according to the very simple rules of the 100-liner gambling contract.


# How do I get set up? ###

* Install geth         
* Wait until it downloads Ethereum blockchain (can take 10 hours or more). 
* Copy and paste the code to Solidity web IDE  https://ethereum.github.io/browser-solidity 
* Press 'Create' button to compile the code, then copy and paste the commands from 'Web3 deploy' section. 
* In terminal, run 
## geth console 

* Create a new Ethereum wallet and unlock it (you will have to unlock it every time you use it)
## personal.newAccount()
## personal.unlockAccount(eth.coinbase)
## eth.defaultAccount = eth.coinbase

* Paste the commands copied in p.4. You will need some small amount of gas to deploy the contract and every time you call roll() and withdraw() functions.

* Wait for 'Contract mined!' message in Ethereum web3 console
* Get some Ethereum and send it to the contract address
## eth.sendTransaction({from:eth.coinbase, to:casino_sol_casino.address, value: web3.toWei(0.1)})


* Let your friends do the same steps excluding those related to deploying contract. They should also send the money to the same contract address.
* Make sure the money arrived (casino_sol_casino is the contract instance name) 
## casino_sol_casino.balance


#Functions to interface with contract:
#free:
## casino_sol_casino.users(0)   - show the first user address
## casino_sol_casino.users(1)   - show the second user address
## casino_sol_casino.balance()   - show your balance
## casino_sol_casino.jackpot()   - show amount of money in game
## casino_sol_casino.winner()   - show the winner
#need gas and wallet unlock:
## casino_sol_casino.roll({gas: 500000})     - choose the winner basing on last mined block hash (may need some extra gas depending on how many players participate) 
## casino_sol_casino.withdraw() - send all your balance back to your Ethereum wallet





Enjoy!